export enum FaceStatus {
  new = "new",
  finished = "finished",
  creator = "creator",
}

export interface IColorTheme {
  theme: number;
  active: string;
  passive: string;
}

export const COLORS: IColorTheme[] = [
  {
    theme: 1,
    active: "#4E9BE2",
    passive: "#CDDFEF",
  },
  {
    theme: 2,
    active: "#50CDCD",
    passive: "#E0F1F1",
  },
  {
    theme: 3,
    active: "#36CC72",
    passive: "#E2F5EA",
  },
  {
    theme: 4,
    active: "#DB9555",
    passive: "#EADFD5",
  },
  {
    theme: 5,
    active: "#BE60CD",
    passive: "#E7DEE9",
  },
  {
    theme: 6,
    active: "#D96F41",
    passive: "#E9DAD4",
  },
  {
    theme: 7,
    active: "#3484E2",
    passive: "#E1E8F0",
  },
  {
    theme: 8,
    active: "#A8B143",
    passive: "#DCDEC5",
  },
  {
    theme: 9,
    active: "#4E9EC0",
    passive: "#C1D5DE",
  },
  {
    theme: 10,
    active: "#CF776A",
    passive: "#E7CCC8",
  },
  {
    theme: 11,
    active: "#367961",
    passive: "#C9E1D8",
  },
  {
    theme: 12,
    active: "#9098C1",
    passive: "#DCDFEF",
  },
  {
    theme: 13,
    active: "#975BE2",
    passive: "#DBCFEA",
  },
  {
    theme: 14,
    active: "#7BB038",
    passive: "#DEE8D2",
  },
  {
    theme: 15,
    active: "#5A58CE",
    passive: "#C8C8E9",
  },
];
