import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import gsap from "gsap";

import InlineSvg from "vue-inline-svg";
import PerfectScrollbar from "vue3-perfect-scrollbar";
import "vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css";
import vfmPlugin from "vue-final-modal";
import VueSnip from "vue-snip";
import { VueWindowSizePlugin } from "vue-window-size/option-api";
import vClickOutside from "click-outside-vue3";

const app = createApp(App);
app.component("inline-svg", InlineSvg);
app.use(store);
app.use(vfmPlugin);
app.use(VueSnip);
app.use(gsap);
app.use(vClickOutside);
app.use(PerfectScrollbar);
app.use(VueWindowSizePlugin);
app.use(router);
app.mount("#app");
