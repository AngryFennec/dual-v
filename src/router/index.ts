import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import Creators from "@/views/Creators.vue";
import Page404 from "@/views/Page404.vue";
import IQ from "@/views/IQ.vue";
import Playground from "@/views/Playground.vue";
import Proposal from "@/views/Proposal.vue";
import Dao from "@/views/Dao.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    components: {
      default: Home,
    },
  },
  {
    path: "/creators",
    name: "Creators",
    components: {
      default: Creators,
    },
  },
  {
    path: "/404",
    name: "Page404",
    components: {
      default: Page404,
    },
  },
  {
    path: "/dao",
    name: "Dao",
    components: {
      default: Dao,
    },
  },
  {
    path: "/iq",
    name: "IQ",
    components: {
      default: IQ,
    },
  },
  {
    path: "/playground",
    name: "Playground",
    components: {
      default: Playground,
    },
  },
  {
    path: "/proposal",
    name: "Proposal",
    components: {
      default: Proposal,
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
