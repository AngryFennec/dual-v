export interface IDaoCard {
  number: number;
  title: string;
  src: string;
  status: string;
  url: string;
}

export const enum EnumVoteCard {
  for = "for",
  against = "against",
  abstain = "abstain",
}
