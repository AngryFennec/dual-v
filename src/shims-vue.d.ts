import { Store } from "@/store"; // path to store file
import mitt from 'mitt';

/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component

  interface ComponentOptions {
    provide?: object | ((this: ComponentPublicInstance) => object)
  }
}

declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: Store;
    emitter: mitt;
  }
}

