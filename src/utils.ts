export const trimString = (str: string, symbols: number) => {
  console.log(str.length);
  return str.length > symbols ? `${str.substring(0, symbols)}...` : str;
};
