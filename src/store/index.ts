import { createStore } from "vuex";
import { COLORS, FaceStatus } from "@/const";

export default createStore({
  state: {
    isAuth: false,
    colorTheme: 11,
    currentFaceStatus: FaceStatus.new,
    isMyBid: true,
  },
  mutations: {
    setAuth(state, payload: boolean) {
      state.isAuth = payload;
    },
    setFaceStatus(state, payload: FaceStatus) {
      state.currentFaceStatus = payload;
    },
    setColor(state, payload: number) {
      const colorObj = COLORS[payload - 1];
      if (colorObj) {
        state.colorTheme = payload;
        document.documentElement.style.setProperty(
          "--active",
          COLORS[state.colorTheme - 1].active
        );
        document.documentElement.style.setProperty(
          "--passive",
          COLORS[state.colorTheme - 1].passive
        );
      }
    },
  },
  actions: {},
  modules: {},
});
