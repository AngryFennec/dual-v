module.exports = {
  devServer: {
    port: 3000,
    disableHostCheck: true,
  },
  publicPath:
    process.env.NODE_ENV === "production"
      ? ""
      : "/",
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
          @import "@/assets/scss/variables.scss";
          @import "@/assets/scss/fonts.scss";
          @import "@/assets/main.scss";
        `,
      },
    },
  },
};
